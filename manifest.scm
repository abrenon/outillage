(use-modules ((geode packages annotation) #:select (python-stanza))
             ((geode packages models) #:select (stanza-fr))
             ((gnu packages commencement) #:select (gcc-toolchain))
             ((gnu packages haskell) #:select (ghc))
             ((gnu packages haskell-web) #:select (ghc-hxt))
             ((gnu packages haskell-xyz) #:select (ghc-attoparsec))
             ((gnu packages python) #:select (python))
             ((gnu packages python-xyz) #:select (python-beautifulsoup4))
             ((gnu packages xml) #:select (python-lxml)))

(packages->manifest
  (list
    coreutils ; mktemp for atomic processing, strip CSV headers, general scripting
    gcc-toolchain ; running haskell
    ghc ; running haskell
    ghc-attoparsec ; parsing metadata
    ghc-hxt ; working on xml documents
    python ; scripts
    python-beautifulsoup4 ; extract EDdA metadata from TEI files
    python-lxml ; fusion articles into tomes for TXM
    python-stanza ; annotation
    stanza-fr ; annotation
    ))
