CORPUS_ROOT=/home/alice/Dehors/Corpus
EDDA=$(CORPUS_ROOT)/EDdA
LGE=$(CORPUS_ROOT)/LGE

EDDA_META=$(EDDA)/metadata.csv

RAW_LGE=$(LGE)/Text
TOME_DIRS=$(wildcard $(RAW_LGE)/T*)
TOMES=$(TOME_DIRS:$(RAW_LGE)/T%=%)

TEI_LGE=$(LGE)/TEI
PARALLEL_LGE=$(LGE)/Parallel
LGE_META_FROM_EDDA=$(PARALLEL_LGE)/metadata.csv
RAW_PARALLEL_LGE=$(PARALLEL_LGE)/Text
LINEARIZED_PARALLEL_LGE_ROOT=$(PARALLEL_LGE)/Linearized
TEI_PARALLEL_LGE=$(PARALLEL_LGE)/TEI

LINEARIZED_PARALLEL_LGE=$(LINEARIZED_PARALLEL_LGE_ROOT)/ $(TOMES:%=$(LINEARIZED_PARALLEL_LGE_ROOT)/T%)
STANZA_PARALLEL_LGE=$(PARALLEL_LGE)/stanza

METADATA=$(EDDA_META) $(LGE_META_FROM_EDDA)

all: $(METADATA) $(LINEARIZED_PARALLEL_LGE)

$(EDDA_META): $(EDDA)/TEI/
	./scripts/EDdA-metadata.py $< > $@

$(LGE_META_FROM_EDDA): $(EDDA_META) $(PARALLEL_LGE)/
	./scripts/LGE-metadata-from-EDdA.py $< $(RAW_LGE) $@

$(RAW_PARALLEL_LGE): $(LGE_META_FROM_EDDA) $(RAW_LGE)
	./scripts/extract-parallel-LGE.sh $^ $@

$(TEI_PARALLEL_LGE): $(LGE_META_FROM_EDDA) $(TEI_LGE)
	./scripts/extract-parallel-LGE.sh $^ $@

$(STANZA_PARALLEL_LGE): $(RAW_PARALLEL_LGE)
	./scripts/stanza-annotator.py $< $@

%/:
	mkdir -p $@

$(LINEARIZED_PARALLEL_LGE_ROOT)/T%: $(RAW_PARALLEL_LGE)/T%
	mkdir -p $@
	find $< -type f -name '*.txt' | ./scripts/linearize.hs $@
