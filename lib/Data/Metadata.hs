{-# LANGUAGE NamedFieldPuns #-}
module Data.Metadata (
      Metadata
    , byTome
    , metadata
  ) where

import Data.Attoparsec.Text (Parser, endOfLine, sepBy1, takeTill)
import Data.List as List (foldl')
import Data.Map as Map (Map, adjust, empty, foldl', insert, member)
import Data.Metadata.Article (Article(..), article)
import Data.Text (Text)

--type Metadata = Map Text Article
type Metadata = [Article]
type Tomes = Map Int Metadata

metadata :: Parser Metadata
--metadata = List.foldl' indexByUid Map.empty <$>
--  (skipLine *> article `sepBy1` endOfLine)
metadata = skipLine *> article `sepBy1` endOfLine
  where
    skipLine = takeTill (\c -> c == '\r' || c == '\n') *> endOfLine
    indexByUid temp a@(Article {uid}) = Map.insert uid a temp

byTome :: Metadata -> Tomes
--byTome = Map.foldl' sortByTome Map.empty
byTome = List.foldl' sortByTome Map.empty
  where
    sortByTome temp a@(Article {uid, tome}) =
--        Map.adjust (Map.insert uid a) tome .
        Map.adjust (a:) tome
--      . (if tome `member` temp then id else Map.insert tome Map.empty) $ temp
      . (if tome `member` temp then id else Map.insert tome []) $ temp
{-
      . (if tome `member` temp then id else Map.insert tome Map.empty)
      $ tome
      -}
