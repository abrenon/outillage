{-# LANGUAGE OverloadedStrings #-}
module Data.Metadata.Article (
      Article(..)
    , article
  ) where

import Data.Attoparsec.Text (Parser, char, decimal, sepBy1, string, takeTill)
import Data.Text (Text, stripEnd)

data Article = Article {
      uid :: Text
    , tome :: Int
    , rank :: Int
    , headWord :: Text
    , domains :: [Text]
  } deriving Show

article :: Parser Article
article = Article
  <$> cell
  <*> (decimal <* char ',')
  <*> (decimal <* char ',')
  <*> cell
  <*> domains_
  where
    cell = takeTill (== ',') <* char ','

domains_ :: Parser [Text]
domains_ = fmap stripEnd <$>
  (takeTill (`elem` ['|', '\r', '\n'])) `sepBy1` (string "| ")
