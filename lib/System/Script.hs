module System.Script (
    syntax
  ) where

import System.Exit (die)
import System.Environment (getProgName)
import Text.Printf (printf)

syntax :: String -> IO ()
syntax s = do
  this <- getProgName
  die $ printf "Syntax: %s %s" this s
