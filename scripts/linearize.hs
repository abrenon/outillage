#!/usr/bin/env -S runhaskell --ghc-arg="-i lib"

import Data.Char (isUpper)
import System.Environment (getArgs)
import System.FilePath ((</>))
import System.Script (syntax)
import Text.Editor (editAll)

linearize :: String -> String
linearize "" = ""
linearize ('¬':'\n':s) = linearize s
linearize ('\n':'\n':s) = "\n\n" ++ linearize s
linearize ('\n':s) = ' ' : linearize s
linearize ('-':'\n':c:s)
  | isUpper c = '-' : c : linearize s
linearize (c:s) = c : linearize s

main :: IO ()
main = getArgs >>= cli
  where
    cli [target] = getContents >>= editAll linearize target
    cli _ = syntax "TARGET_DIR"
