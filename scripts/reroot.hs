#!/usr/bin/env -S runhaskell --ghc-arg="-i lib"
{-# LANGUAGE OverloadedStrings #-}

import Data.Text (Text)
import System.Environment (getArgs)
import System.Script (syntax)
import Text.Editor (editAll)

wrap :: [Text] -> [Text]
wrap text = "<body>": (("\t"<>) <$> text) ++ ["</body>"]

main :: IO ()
main = getArgs >>= run
  where
    run [target] = getContents >>= editAll wrap target
    run _ = syntax "TARGET_DIRECTORY"
