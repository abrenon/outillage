#!/bin/sh

INPUT_DIR="${1%/}"
TOME_NUMBER="${INPUT_DIR##*/T}"
TOME="T${TOME_NUMBER}"
OUTPUT="${2%/}/${TOME}.xml"

cat <<EOF > "${OUTPUT}"
<teiCorpus version="3.3.0" xmlns="http://www.tei-c.org/ns/1.0">
<teiHeader>
  <fileDesc>
    <titleStmt>
      <title>
		La Grande Encyclopédie, Inventaire raisonné des sciences, des lettres et
		des arts par une société de savants et de gens de lettres. Tome ${TOME_NUMBER}.
      </title>
     <respStmt>
      <resp>Digitized by</resp>
      <orgName>
		Bibliothèque Nationale de France
      </orgName>
      <resp>Encoded by</resp>
      <orgName>
		ICAR
      </orgName>
     </respStmt>
    </titleStmt>
    <publicationStmt>
      <distributor>
       <orgName>
        Project GÉODE
       </orgName>
       <address>
        <addrline>
         ICAR UMR 5191
        </addrline>
        <addrline>
         ENS de Lyon
        </addrline>
       </address>
      </distributor>
    </publicationStmt>
    <sourceDesc>
      <bibl>
       <title>
		La Grande Encyclopédie, Inventaire raisonné des sciences, des lettres et
		des arts par une société de savants et de gens de lettres. Tome ${TOME_NUMBER}.
       </title>
       <author>
        Collective
       </author>
       <creation>
        <date>
         1885
        </date>
       </creation>
       <imprint>
        <date>
         1885
        </date>
        <publisher>
         H. Lamirault et Cie,
        </publisher>
        <pubplace>
         Paris
        </pubplace>
       </imprint>
       <biblScope unit="volume">${TOME_NUMBER}</biblScope>
      </bibl>
    </sourceDesc>
  </fileDesc>
</teiHeader>
EOF

find "${INPUT_DIR}" -type f -name "*.tei" | python3 "${0%/*}/reimport.py" >> "${OUTPUT}"

cat <<EOF >> "${OUTPUT}"
</teiCorpus>
EOF
