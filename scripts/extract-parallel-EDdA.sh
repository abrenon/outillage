#!/bin/bash

INPUT_METADATA="${1}"
SOURCE_TEXT_ARTICLES="${2}"
OUTPUT="${3}"
if [ -d "${OUTPUT}" ]
then
	N=1
	while [ -d "${OUTPUT}.${N}" ]
	do
		N=$((N+1))
	done
	mv "${OUTPUT}" "${OUTPUT}.${N}"
fi

WORKDIR=$(mktemp -d /tmp/parallel-EDdA.XXX)

for T in {1..17}
do
	mkdir -p "${WORKDIR}/T${T}"
done

while read LINE
do
	LINE="${LINE#*,}"
	LINE="${LINE#*,}"
	LINE="${LINE#*,}"
	LINE="${LINE#*,}"
	T="${LINE%%,*}"
	LINE="${LINE#*,}"
	RANK="${LINE%%,*}"
	cp "${SOURCE_TEXT_ARTICLES}/T${T}/article${RANK}."* "${WORKDIR}/T${T}"
done < <(tail -n +2 ${INPUT_METADATA})

mv ${WORKDIR} ${OUTPUT}
